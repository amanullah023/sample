import json,threading

from channels.layers import get_channel_layer
from asgiref.sync import async_to_sync

from .convert import ConvertFileAPI



class ProgressThread(threading.Thread):
    def __init__(self,file_name,file_type,file_id):
        self.file_name = file_name
        self.file_type = file_type
        self.file_id = file_id
        threading.Thread.__init__(self)
    
    def run(self):
        try:
            print("Thread Execution started...!")
            # here we will send data to FE about current progress.
            file_convertor = ConvertFileAPI(self.file_name,self.file_type,self.file_id)
            file_convertor.convert()
                          
        except Exception as e:
            self.send_progress(-1,'')
            print("Exception >>>",e)
    
    def send_progress(self,progress,link):
        channel_layer = get_channel_layer()
        async_to_sync (channel_layer.group_send)(
            f"progress_{self.file_id}",{
                'type':'send_progress', # function name in consumers.py file
                'value':json.dumps({"counter":progress,"link":link}) # data you want to send.
            }
        )
    