from django.core.files.storage import FileSystemStorage

from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status

from .serializers import DocumentSerializer
from convertor.thread import ProgressThread


@api_view(['POST'])
def DocumentPostView(request):
    try:
        serializer = DocumentSerializer(data=request.data)
        if serializer.is_valid():
            myfile = request.data.get('docfile','')
            selected_type = request.data.get('certificate_type','')
            fs = FileSystemStorage()
            fileName = fs.save('input/'+myfile.name, myfile)
            file_name = fileName[6:].split('.')[0]

            # websocket thread
            ProgressThread(myfile.name,selected_type,file_name).start()
            context = {'message':'Processing started','file_name':file_name}
            return Response(data=context,status=status.HTTP_200_OK)
          
        return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)

    except:
        return Response({'error':'Unexpected error occur'},status=status.HTTP_400_BAD_REQUEST)