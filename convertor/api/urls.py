from django.urls import path

from .views import DocumentPostView

urlpatterns = [
   path('',DocumentPostView,name='Document-Post'),
]