from django.db import models

choices=[('sch_expo_car','sch_expo_car'),
         ('sch_expo_truck','sch_expo_truck'),
         ('pre_expo_car_1','pre_expo_car_1'),
         ('pre_expo_car_2','pre_expo_car_2'),
         ('pre_expo_truck_1','pre_expo_truck_1'),
         ('pre_expo_truck_2','pre_expo_truck_2')
         ]

         
class Document(models.Model):
    docfile = models.FileField(upload_to='documents/')
    certificate_type = models.CharField(default='sch_expo_car',verbose_name="Certificate Type",max_length=50,choices=choices)
    resultfile = models.FileField(null=True,upload_to='output/',blank=True)