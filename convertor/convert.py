### 

import os,json,re,sys,cv2
import pandas as pd
import numpy as np
from pdf2image import convert_from_path

try:
    from PIL import Image
except ImportError:
    import Image
from easyocr import Reader

from django.core.files import File
from channels.layers import get_channel_layer
from asgiref.sync import async_to_sync

from rest_framework.response import Response
from rest_framework import status

from .models import Document


class ConvertFile:
    def __init__(self,f_name,type,file_id):
        """ 
        @response_type 
            1- Web
            2- Api 
        """
        self.f_name = f_name
        self.type = type
        self.file_id = file_id
        self.eng_reader = Reader(['en'], gpu=True)
        self.jpn_reader = Reader(['ja', 'en'], gpu=True)
        self.allow_digit = "0123456789"
        self.allow_float = "0123456789."
        self.allow_char = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        self.allow_digit_char = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_"

        self.base_path = os.path.abspath(os.getcwd())
        self.final_list = []
        self.cert_data = {
        " ": [],
        "車体番号": [],
        "初年度登録年月": [],
        "原動機の型式": [],
        "  ": [],
        "   ": [],
        "総排気量又は規格出力": [], 
        "    ": [],
        "燃料の種別": [],
        "人数": [],
        "     ": [],
        "      ": [],
        "車輛重量": [],
        "       ": [],
        "車輛総重量": [],
        "        ": [],
        "         ": [],
        "長さ": [],
        "          ": [],
        "幅": [],
        "           ": [],
        "高さ": []
        }
        self.cert_columns = [
        " ",
        "車体番号",
        "初年度登録年月",
        "原動機の型式",
        "  ",
        "   ",
        "総排気量又は規格出力",
        "    ",
        "燃料の種別",
        "人数",
        "     ",
        "      ",
        "車輛重量",
        "       ",
        "車輛総重量",
        "        ",
        "         ",
        "長さ",
        "          ",
        "幅",
        "           ",
        "高さ"
    ]
    
    def align_image(self,total_pages, template_file):
        # Open the image files.
        template = cv2.imread(template_file)  # Image to be aligned.
        for i in range(total_pages):
            image_path = self.base_path+f'/pages/cv_inverted_{self.file_id}' + str(i) + '.TIF'
            img1_color = cv2.imread(image_path)  # Image to be aligned.
            # Convert to grayscale.
            img1 = cv2.cvtColor(img1_color, cv2.COLOR_BGR2GRAY)
            img2 = cv2.cvtColor(template, cv2.COLOR_BGR2GRAY)
            height, width = img2.shape

            # Create ORB detector with 5000 features.
            orb_detector = cv2.ORB_create(5000)

            # Find keypoints and descriptors.
            # The first arg is the image, second arg is the mask
            #  (which is not reqiured in this case).
            kp1, d1 = orb_detector.detectAndCompute(img1, None)
            kp2, d2 = orb_detector.detectAndCompute(img2, None)

            # Match features between the two images.
            # We create a Brute Force matcher with
            # Hamming distance as measurement mode.
            matcher = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)

            # Match the two sets of descriptors.
            matches = matcher.match(d1, d2)

            # Sort matches on the basis of their Hamming distance.
            matches.sort(key=lambda x: x.distance)

            # Take the top 90 % matches forward.
            matches = matches[:int(len(matches) * 90)]
            no_of_matches = len(matches)

            # Define empty matrices of shape no_of_matches * 2.
            p1 = np.zeros((no_of_matches, 2))
            p2 = np.zeros((no_of_matches, 2))

            for j in range(len(matches)):
                p1[j, :] = kp1[matches[j].queryIdx].pt
                p2[j, :] = kp2[matches[j].trainIdx].pt

            # Find the homography matrix.
            homography, mask = cv2.findHomography(p1, p2, cv2.RANSAC)

            # Use this matrix to transform the
            # colored image wrt the reference image.
            transformed_img = cv2.warpPerspective(
                img1_color, homography, (width, height))

            # Save the output.
            cv2.imwrite(f'pages/image_{self.file_id}' + str(i) + '.TIF', transformed_img)
        for i in range(total_pages):
            os.remove(self.base_path+f'/pages/cv_inverted_{self.file_id}' + str(i) + '.TIF')

    def pdf_to_images(self,pdf_path):
        pages = convert_from_path(pdf_path, dpi=400)
        for i in range(len(pages)):
            # Send Progress
            stage = 'Extracting certificate images from PDF File. ' + '(' + str(i+1) + '/' + str(len(pages)) + ')'
            self.send_progress(i+1,'', stage)
            # Save pages as images in the pdf
            pages[i].save(f'pages/{self.file_id}' + str(i) + '.TIF', 'TIFF')

            file = self.base_path + f'/pages/{self.file_id}' + str(i) + '.TIF'
            img = cv2.imread(file, 0)
            # new_image = cv2.resize(img, (0, 0), fx=2, fy=2, interpolation=cv2.INTER_CUBIC)
            _, blackAndWhite = cv2.threshold(img, 155, 255, cv2.THRESH_BINARY)
            nlabels, labels, stats, centroids = cv2.connectedComponentsWithStats(blackAndWhite, None, None, None, 8,
                                                                                cv2.CV_32S)
            sizes = stats[1:, cv2.CC_STAT_AREA]  # get CC_STAT_AREA component
            img2 = np.zeros((labels.shape), np.uint8)

            for j in range(0, nlabels - 1):
                if sizes[j] >= 5:  # filter small dotted regions
                    img2[labels == j + 1] = 255

            cv2.imwrite(self.base_path + f'/pages/cv_inverted_{self.file_id}' + str(i) + '.TIF', img2)
        for i in range(len(pages)):
            os.remove(self.base_path + f'/pages/{self.file_id}' + str(i) + '.TIF')
        return pages   
    
    def write_to_list(self,df,index):
        convertor_dict = {
            '車体番号':'chassis_no',
            '初年度登録年月':'first_year',
            '原動機の型式':'model_no',
            '総排気量又は規格出力':'cc',
            '燃料の種別':'fuel',
            '人数':'seats',
            '車輛重量':'net_weight',
            '車輛総重量':'g_weight',
            '長さ':'length',
            '幅':'width',
            '高さ':'height'
        }

        headers = df.columns.values.tolist()
        row = df.iloc[index].values.tolist()
        data = {}
        
        for i,v in enumerate(row):
            header = convertor_dict.get(headers[i],' ') 
            data[header] = v

        self.final_list.append(data)

    def cleanup_text(self,text):
        # strip out non-ASCII text so we can draw the text on the image
        # using OpenCV
        return "".join([c if ord(c) < 128 else "" for c in text]).strip()

    def only_digit(self,data):
        return ''.join(re.findall(r'\d+', data))

    def only_digit_char(self,data):
        return re.sub('[^A-Za-z0-9]+', '', data)
    
    def only_digit_par(self,data):
        return re.sub('[^0-9(-)]+', '', data)

    def trim_text(self,text):
        clean_text = ""
        flag = True
        for i in range(len(text)):
            if flag:
                if text[i] == "-":
                    flag = False
                clean_text = clean_text + text[i]
            else:
                if text[i] != "-":
                    clean_text = clean_text + text[i]
        return clean_text

    def clean_up_temp_files(self,total_pages):
        for i in range(total_pages):
            os.remove(self.base_path+f'/pages/image_{self.file_id}' + str(i) + '.TIF')
        # this will remove file after processing.
        os.remove(self.base_path+f'/media/input/{self.file_id}'+'.pdf')

    def send_progress(self,progress,link, stage):
        channel_layer = get_channel_layer()
        async_to_sync (channel_layer.group_send)(
            f"progress_{self.file_id}",{
                'type':'send_progress', # function name in consumers.py file
                'value':json.dumps({"certificate_no":progress,"data":link, "stage":stage}) # data you want to send.
            }
        )

    def extract_data(self,total_page, filename, type):
        for c,i in enumerate(range(total_page)):
            img = cv2.imread(self.base_path+f'/pages/image_{self.file_id}' + str(i) + '.TIF')
            stage = 'Reading data from certificates ' + '(' + str(i+1) + '/' + str(total_page) + ')'
            self.send_progress(i+1,'', stage)
            coord = []
            if type == 'sch_expo_car':
                coord = [
                    img[950:1060, 235:1400],  # Makers Serial
                    img[710:770, 2310:2535],  # Year
                    img[700:770, 2536:2655],  # Month
                    img[1220:1325, 1750:2050],  # Engine Model
                    img[1240:1315, 2830:3035],  # Engine Cap
                    img[1220:1313, 2360:2670],  # Fuel Class
                    img[905:1055, 2165:2285],  # Fixed Number
                    img[970:1055, 2920:3035],  # Weight
                    img[910:1055, 3360:3565],  # G\Weight
                    img[960:1055, 3675:3800],  # Height
                    img[975:1049, 3950:4068],  # width
                    img[960:1049, 4210:4346]  # Length
                ]
            elif type == 'sch_expo_truck':
                coord = [
                    img[448:527, 2800:3810],  # Maker's Sr. No
                    img[460:560, 2185:2365],  # First Reg. Date P1
                    img[470:530, 2500:2605],  # First Reg. Date P2
                    img[650:750, 3500:3900],  # Engine Model    eng_reader
                    img[1870:1940, 510:675],  # Engine Cap.
                    img[1870:1938, 695:960],  # Fuel Classification.
                    img[1600:1670, 2670:2725],  # Fixed Number
                    img[1600:1665, 3550:3770],  # Weight
                    img[1585:1665, 4250:4450],  # G/Weight eng_reader
                    img[1850:1930, 2500:2725],  # Height
                    img[1850:1930, 2825:3030],  # Width
                    img[1850:1930, 3150:3340]  # Length
                ]
            elif type == 'pre_expo_car_1':
                coord = [
                    img[980:1070, 200:1600],  # Makers Serial
                    img[775:840, 2340:2420],  # Year
                    img[750:840, 2525:2640],  # Month
                    img[1200:1290, 1720:1900],  # Engine Model
                    img[1200:1290, 2750:2907],  # Engine Cap
                    img[1200:1290, 2300:2600],  # Fuel Class
                    img[980:1070, 2130:2280],  # Fixed Number
                    img[980:1070, 2785:2910],  # Weight
                    img[980:1070, 3350:3510],  # G\Weight
                    img[980:1070, 3650:3780],  # Height
                    img[980:1070, 3925:4050],  # width
                    img[980:1070, 4200:4320]  # Length
                ]
            elif type == 'pre_expo_car_2':
                coord = [
                    img[980:1080, 210:1300],  # Makers Serial
                    img[775:850, 2350:2530],  # Year
                    img[775:850, 2540:2660],  # Month
                    img[1200:1300, 1740:1980],  # Engine Model
                    img[1200:1295, 2750:2929],  # Engine Cap
                    img[1200:1295, 2300:2600],  # Fuel Class
                    img[1000:1075, 2175:2250],  # Fixed Number
                    img[1000:1075, 2775: 2935],  # Weight
                    img[1000:1068, 3350:3530],  # G\Weight
                    img[990:1065, 3680:3795],  # Height
                    img[990:1065, 3940:4068],  # width
                    img[990:1065, 4210:4335]  # Length
                ]
            elif type == 'pre_expo_truck_1':
                coord = [
                    img[610:700, 2335:3300],  # Maker's Sr. No
                    img[640:697, 1940:2054],  # First Reg. Date P1
                    img[640:697, 2150:2222],  # First Reg. Date P2
                    img[800:920, 3610:3950],  # Engine Model    eng_reader
                    img[1500:1568, 500:689],  # Engine Cap.
                    img[1500:1567, 716:970],  # Fuel Classification.
                    img[1290:1361, 2670:2775],  # Fixed Number
                    img[1280:1356, 3640:3813],  # Weight
                    img[1280:1355, 4325:4492],  # G/Weight eng_reader
                    img[1480:1561, 2610:2745],  # Height
                    img[1480:1560, 2930:3055],  # Width
                    img[1480:1560, 3240:3367]  # Length
                ]
            elif type == 'pre_expo_truck_2':
                coord = [
                    img[940:1035, 149:900],  # Maker's Sr. No
                    img[570:628, 1950:2045],  # First Reg. Date P1
                    img[570:628, 2130:2215],  # First Reg. Date P2
                    img[1160:1225, 1417:1700],  # Engine Model    eng_reader
                    img[1160:1225, 2575:2750],  # Engine Cap.
                    img[1160:1220, 2775:3060],  # Fuel Classification.
                    img[750:825, 2680:2770],  # Fixed Number
                    img[750:828, 3600:3800],  # Weight
                    img[750:820, 4300:4477],  # G/Weight eng_reader
                    img[960:1023, 2600:2736],  # Height
                    img[960:1023, 2920:3045],  # Width
                    img[960:1023, 3230:3355]  # Length
                ]

            # if i == 8:
            #     plotting = plt.imshow(img[8])
            #     plt.show()

            # Maker Serial Number
            makerSerial = self.trim_text(
                ''.join(self.eng_reader.readtext(coord[0], detail=0, allowlist=self.allow_digit_char,
                                            paragraph=False)).replace(' ', '').replace('_', '-'))
            #
            # First Grant Date
            year = self.only_digit("".join(self.jpn_reader.readtext(
                coord[1], detail=0, paragraph=False)))
            month = self.only_digit("".join(self.jpn_reader.readtext(
                coord[2], detail=0, paragraph=False, text_threshold=0.1, low_text=0.1)))

            if month != "" and int(month) <= 9:
                month = "0" + month
            firstRegDate = year + month

            #
            # Engine Model
            engineModel = self.only_digit_char(
                "".join(self.eng_reader.readtext(coord[3], detail=0, paragraph=False)))

            # Engine Cap.
            engCap = self.only_digit("".join(self.jpn_reader.readtext(
                coord[4], detail=0, paragraph=False)))
            if engCap != '':
                engCap = engCap[0] + '.' + engCap[1:]
                engCap = float(engCap) * 1000

            # Fuel Class
            fuelClass = "".join(self.jpn_reader.readtext(
                coord[5], detail=0, paragraph=False, allowlist="ガソリン軽油"))
            if fuelClass.__contains__('ガ') or fuelClass.__contains__('ソ') or fuelClass.__contains__(
                    'リ') or fuelClass.__contains__('ン'):
                fuelClass = "ガソリン"
            elif not fuelClass:
                fuelClass = ""    
            else:
                fuelClass = "軽油"

            # Fixed Number
            fixedNumber = self.only_digit_par(
                "".join(self.jpn_reader.readtext(coord[6], detail=0, paragraph=False)))

            # Weight
            if type == 'pre_expo_truck_1':
                weight = self.only_digit("".join(self.jpn_reader.readtext(coord[7], text_threshold=0.1, low_text=0.1,
                                                                detail=0, paragraph=False)))
            else:
                weight = self.only_digit(
                    "".join(self.jpn_reader.readtext(coord[7], detail=0, paragraph=False)))
            # Gweight
            gWeight = self.only_digit_par(
                "".join(self.eng_reader.readtext(coord[8], detail=0, paragraph=False)))           

            if type == 'sch_expo_truck':
                if int(gWeight) < int(weight) and int(gWeight[0]) == 1:
                    gWeight = '4' + gWeight[1:]

            if engineModel == 'KGA':
                engineModel = 'K6A'
            elif engineModel == 'ROGA' or engineModel == 'RO6A':
                engineModel = 'R06A'

            # Height
            height = self.only_digit("".join(self.eng_reader.readtext(
                coord[9], detail=0, paragraph=False)))[:3]

            # Width
            width = self.only_digit("".join(self.eng_reader.readtext(
                coord[10], detail=0, paragraph=False)))[:3]

            # Length
            length = self.only_digit("".join(self.eng_reader.readtext(
                coord[11], detail=0, paragraph=False)))[:3]

            self.cert_data[self.cert_columns[0]].append(0)

            self.cert_data[self.cert_columns[1]].append(
                makerSerial
            )
            self.cert_data[self.cert_columns[2]].append(
                firstRegDate
            )
            self.cert_data[self.cert_columns[3]].append(
                engineModel
            )

            # Empty Column
            self.cert_data[self.cert_columns[4]].append("   ")

            # Empty Column
            self.cert_data[self.cert_columns[5]].append("   ")

            # Engine Cap.
            self.cert_data[self.cert_columns[6]].append(
                engCap
            )

            # Empty Column
            self.cert_data[self.cert_columns[7]].append("   ")

            # Fuel Classification
            self.cert_data[self.cert_columns[8]].append(
                fuelClass
            )

            # Fixed Number

            self.cert_data[self.cert_columns[9]].append(
                fixedNumber
            )

            # Empty Column
            self.cert_data[self.cert_columns[10]].append("   ")
            # Empty Column
            self.cert_data[self.cert_columns[11]].append("   ")

            # Weight
            self.cert_data[self.cert_columns[12]].append(
                weight
            )

            # Empty Column
            self.cert_data[self.cert_columns[13]].append("   ")

            # G/Weight
            self.cert_data[self.cert_columns[14]].append(
                gWeight
            )

            # Empty Column
            self.cert_data[self.cert_columns[15]].append("   ")
            # Empty Column
            self.cert_data[self.cert_columns[16]].append("   ")

            # Height
            self.cert_data[self.cert_columns[17]].append(
                height
            )
            # Empty Column
            self.cert_data[self.cert_columns[18]].append("   ")

            # Width
            self.cert_data[self.cert_columns[19]].append(
                width
            )
            # Empty Column
            self.cert_data[self.cert_columns[20]].append("   ")

            # Length
            self.cert_data[self.cert_columns[21]].append(
                length
            )

            c_data = pd.DataFrame.from_dict(self.cert_data)
            c_data = c_data[self.cert_columns]

            self.write_to_list(c_data,c)
        
        # clean up temp files
        self.clean_up_temp_files(total_page) 
                
        return self.final_list


class ConvertFileAPI:
    def __init__(self,file_name,file_type,file_id):
        self.file_name = file_name
        self.file_type = file_type
        self.file_id = file_id
    
    def send_progress(self,progress,link, stage):
        channel_layer = get_channel_layer()
        async_to_sync (channel_layer.group_send)(
            f"progress_{self.file_id}",{
                'type':'send_progress', # function name in consumers.py file
                'value':json.dumps({"counter":progress,"data":link, "stage":stage}) # data you want to send.
            }
        )
    
    def convert(self):
        try:
            # object instantiation
            file_convertor = ConvertFile(self.file_name,self.file_type,self.file_id)

            # config
            type_list = ['sch_expo_car', 'sch_expo_truck', 'pre_expo_car_1',
                        'pre_expo_car_2', 'pre_expo_truck_1', 'pre_expo_truck_2']
            if self.file_type in type_list:
                template = file_convertor.base_path + '/files/' + self.file_type + '.TIF'
            else:
                print("Invalid certificate type")
                sys.exit()
            
            
            # method calling
            self.send_progress(1,'', '')
            f_name = os.path.splitext(os.path.basename(self.file_name))[0]

            self.send_progress(2,'', 'Extracting certificate images from PDF file.')
            total_pages = file_convertor.pdf_to_images('media/input/' + self.file_name)

            self.send_progress(3,'', 'Processing certificate images.')
            file_convertor.align_image(len(total_pages), template)

            self.send_progress(4,'', 'Reading data from certificates.')
            final_list = file_convertor.extract_data(len(total_pages), f_name, self.file_type)

            self.send_progress(5,final_list, '')

            return final_list
        except Exception as e:
            print(e)
            return Response(e,status=status.HTTP_400_BAD_REQUEST)


    
