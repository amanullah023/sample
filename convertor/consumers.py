""" This is much like views in standard django request/response but this is statefull """
import json

from channels.generic.websocket import AsyncJsonWebsocketConsumer,WebsocketConsumer
from asgiref.sync import async_to_sync,sync_to_async



class ProgressConsumer(AsyncJsonWebsocketConsumer):

    async def connect(self):
        self.room_name = self.scope['url_route']['kwargs']['file_name']
        self.room_group_name = f"progress_{self.room_name}"
        await (self.channel_layer.group_add)( self.room_group_name,self.channel_name)
        await  self.accept()
        await self.send(text_data=json.dumps({'status':f'connected to progress consumer {self.room_name}'}))
    
    async def receive(self, text_data):
        await self.send(text_data=json.dumps({"message":"Data Received...!"}))
        
    
    async def disconnect(self, *args, **kwargs):
        await self.send(text_data=json.dumps({'status':'Disconnected'}))
    
    async def send_progress(self,event):
        data = json.loads(event.get('value'))
        await self.send(text_data=json.dumps({'payload':data}))
    