# export_certificates

1) Clone the APP
2) Create virtual Environment with Python 3.8
3) Install requirements through requirements.txt file.
4) Run the app

### API for convertor
url POST -> localhost:8000/api/convertor/

payload 
{
    "docfile":pdf file,
    "certificate_type":"certificate type"
}
Availabe certificate types
    --> 'sch_expo_car'
    --> 'sch_expo_truck'
    --> 'pre_expo_car_1'
    --> 'pre_expo_car_2'
    --> 'pre_expo_truck_1'
    --> 'pre_expo_truck_2'

Response

{
  "message": "Processing started",
  "file_name": "trucks_hJvKidt"
}

Now you can access progress of image processing using file_name. see below example
url -> ws://localhost:8000/ws/progress/file_name
e.g -> ws://localhost:8000/ws/progress/trucks_hJvKidt

payload of websocket for progress bar.

{
  "payload": {
    "counter": 1,
    "data": ""
  }
}

When the counter reaches to 5  payload will be.

{   
  payload:{
    "counter": 5,
    "data":[  Array of objects 
      {
          " ": "   ",
          "chassis_no": "FRR90-7058035",
          "First year registration date": "201401",
          "model_no": "4HK1",
          "cc": 5190.0,
          "fuel": "軽油",
          "seats": "2",
          "net_weight": "4780",
          "g_weight": "7990",
          "length": "850",
          "width": "249",
          "height": "320"
      },..
      ]
  }
}



you can test progress with following tool.
--> https://websocketking.com/
 or you can use your prefered tool.

 

