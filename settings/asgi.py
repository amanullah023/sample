"""
ASGI config for settings project.

It exposes the ASGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.2/howto/deployment/asgi/
"""
# language imports
import os

# django imports
import django
from django.core.asgi import get_asgi_application
from django.core.wsgi import get_wsgi_application
from django.urls import path

# third party imports
from channels.routing import ProtocolTypeRouter,URLRouter
from channels.http import AsgiHandler

# custom imports
from convertor import consumers


os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'settings.settings')
django.setup()

application = get_asgi_application()

ws_patterns = [
    path('ws/progress/<file_name>', consumers.ProgressConsumer.as_asgi())
]

application = ProtocolTypeRouter({
    # Django's ASGI application to handle traditional HTTP requests
    "http": AsgiHandler(),

    'websocket': URLRouter(ws_patterns)
})