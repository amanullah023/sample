# Base Image
FROM python:3.8-slim-buster


ENV PYTHONUNBUFFERED=1
RUN mkdir /app
WORKDIR /app
RUN apt-get update && apt-get install -y wget libgl1-mesa-glx libsm6 libxext6 \
    libfontconfig1 libxrender1 \
    libglib2.0-0 poppler-utils \
    && apt-get clean && rm -rf /var/lib/apt/lists/*

COPY requirements.txt /app/
RUN pip install -r requirements.txt --no-cache-dir
RUN pip install uvicorn[standard]
ADD . /app/
